var mainExecutor= require('../executor/mainExecutor.js');
var stdin = process.openStdin();

process.stdout.write("Input>>");

stdin.addListener("data", function(input) {
  // note:  d is an object, and when converted to a string it will
  // end with a linefeed.  so we (rather crudely) account for that  
  // with toString() and then trim() 
  input = input.toString().trim();

  var messageJson = {
    message: input,
    senderId: "1135193663199414",
    platform: "FACEBOOK"
  };

  mainExecutor(messageJson);

});