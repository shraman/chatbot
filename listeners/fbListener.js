var express = require('express');
var bodyParser = require('body-parser');
var prettyjson = require('prettyjson');
var mainExecutor= require('../executor/mainExecutor.js');

var app = express();


function Listen(){

  app.use(bodyParser.json());

  app.use(bodyParser.urlencoded({
    extended: true
  }));

  app.get('/test_node/', function (req, res) {
    if (req.query['hub.verify_token'] === 'hello') {
      res.send(req.query['hub.challenge']);
    }
    res.send('Error, wrong validation token');
  });

  app.post('/test_node/', function (req, res) {
    messaging_events = req.body.entry[0].messaging;
    for (i = 0; i < messaging_events.length; i++) {
      var event = req.body.entry[0].messaging[i];
      var senderId = event.sender.id;
      if (event.message && event.message.text) {
        text = event.message.text;
      } else if(event.postback && event.postback.payload){
        text = event.postback.payload;
      }
      var messageJson = {
        message: text.toLowerCase(),
        senderId: senderId,
        platform: "FACEBOOK"
      };

      mainExecutor(messageJson);
    }
    console.log(prettyjson.render(req.body));
    res.sendStatus(200);
  });

  app.listen(8001, function () {
    console.log('App listening on port 8001!');
  });

}

module.exports = Listen;