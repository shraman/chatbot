var prettyjson = require('prettyjson');
var messages = require('../executor/staticMessages/genericMessages.js');

function sendResponse(response){
  switch(response.type){
    case "PLAIN_TEXT":
      console.log(response.message);
      console.log('------------------------');
      break;
    case "ARRAY":
      sendArray(response);
      console.log('------------------------');
      break;
    case "CART":
      sendCart(response);
      console.log('------------------------');
      break;
    case "BILL":
      sendBill(response);
      console.log('------------------------');
      break;
  }
}


function sendArray(response){
  // console.log(response);
  array = response.array;
  if(response.message){
    console.log(response.message);
  }
  console.log("Select");
  var j;
  for(var i in array){
    j = parseInt(i)+1;
    console.log(j+")"+ array[i]);
  }
}

function sendCart(response){
  console.log(prettyjson.render(response.cart));
}

function sendBill(response){
  console.log(messages.confirmOrder);
  console.log(prettyjson.render(response.cart));
  console.log("price",response.price);
  console.log("tax",response.tax);
  console.log("total",response.total);
}

module.exports = sendResponse;