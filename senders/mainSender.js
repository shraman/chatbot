var fbSender = require('./fbSender.js');
var consoleSender = require('./consoleSender.js');

function sendMessage(response){
  switch(response.platform){
    case "CONSOLE":
      consoleSender(response);
      break;
    case "FACEBOOK":
      fbSender(response);
      break;    
  }
}

module.exports = sendMessage;