var request = require('request');
var prettyjson = require('prettyjson');
var messages = require('../executor/staticMessages/genericMessages.js');
var token = "EAAG88IDttSkBAFD43adsnmp136B4ZCliRIrWeOqEGQKaGgMdZC7VaFqAvmsbLKCDWhxevL7LZARdnKvZAfZCsVqlPQ0txYp8RCZAaxYSJCBpVIq4zbL8ocdZBRKWNGRhzuy3eaiqn88Y13IMj9igy97MvhAY4KWbxyU0csIWEhg5AZDZD";

String.prototype.toProperCase = function() {
  return this.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
};

function sendResponse(response){
  switch(response.type){
    case "PLAIN_TEXT":
      sendPlainText(response);
      break;
    case "ADDRESSES":
      sendAddresses(response);
      break;
    case "LOCALITIES":
      sendLocalities(response);
      break;
    case "ADDRESS_CONFIRMATION":
      sendAddressConfirmation(response);
      break;
    case "RESTAURANTS":
      sendRestaurants(response);
      break;
    case "ITEMS":
      sendItems(response);
      break;
    case "ARRAY":
      sendArray(response);
      break;
    case "CART":
      sendCart(response);
      break;
    case "CONFIRMATION_RECEIPT":
      sendConfirmationReceipt(response);
      break;
    case "ORDER_RECEIPT":
      sendOrderReceipt(response);
      break;
    case "ENTER_OTP":
      sendEnterOtp(response);
      break;
  }
}

function sendPlainText(response){
  if(response.messageExtra) setTimeout(sendTextMessage,0,response.senderId,response.messageExtra); 
  setTimeout(sendTextMessage,500,response.senderId,response.message);
}

function sendAddressConfirmation(response){
  sendConfirmationButtonTemplate(response.senderId,response.message,"Confirm address","Re-enter");
}

function sendEnterOtp(response){
  var buttonsData = [];
  buttonsData.push({
    type:"postback",
    title: "Resend OTP",
    payload: "resend"
  });
  sendButtonTemplate(response.senderId,response.message,buttonsData);
}

function sendItems(response){
  var items = [];
  var item;
  var temp;
  var description;
  var title;
  // console.log(response);
  for(var i in response.items){
    temp = parseInt(i)+1;
    title = response.items[i].name.toProperCase() + "        Rs." + response.items[i].price;
    if(response.items[i].size && response.items[i].size !== "NONE") title += " for "+ response.items[i].size.toProperCase();
    description =  ( response.items[i].veg_type == "VEG" ? "Vegetarian" : "Non-vegetarian") + " dish";
    if(response.items[i].description !== "") description+= ", " + response.items[i].description.toProperCase();
    item = {
      title: title,
      subtitle: description,
      image_url: response.items[i].image_url,
      buttons: [{
        type:"postback",
        title: "Add",
        payload: "add "+ temp
      },{
        type:"postback",
        title: "Remove",
        payload: "rem "+ temp
      }]
    };
    items.push(item);
  }
  if(response.messageAbove) sendTextMessage(response.senderId, response.messageAbove);
  if(response.message) setTimeout(sendTextMessage,250,response.senderId, response.message);
  setTimeout(sendGenericMessage,500,response.senderId,items);
  if(response.messageBelow) setTimeout(sendTextMessage,1000,response.senderId, response.messageBelow);
}

function sendLocalities(response){
  var items = [];
  var item;
  for(var i in response.localities){
    item = {
      title: response.localities[i].terms[0].value,
      subtitle: response.localities[i].description,
      buttons: [{
        type:"postback",
        title: "Select",
        payload: parseInt(i)+1
      }]
    };
    items.push(item);
  }
  if(response.message) sendTextMessage(response.senderId, response.message);
  sendGenericMessage(response.senderId,items);
  setTimeout(sendTextMessage,1000,response.senderId, messages.generateTipsMessage(['searchLocalityTip']));
}

function sendRestaurants(response){
  var items = [];
  var item;
  for(var i in response.restaurants){
    item = {
      title: response.restaurants[i].name.toProperCase(),
      buttons: [{
        type:"postback",
        title: "Select",
        payload: parseInt(i)+1
      }]
    };
    if(response.restaurants[i].image_url) item.image_url = response.restaurants[i].image_url.replace("medium_square","180x180_jpeg");
    items.push(item);
  }
  if(response.message) sendTextMessage(response.senderId, response.message);
  setTimeout(sendTextMessage,250,response.senderId, messages.selectRestaurant);
  setTimeout(sendGenericMessage,500,response.senderId,items);
}

function sendAddresses(response){
  var items = [];
  var item;
  for(var i in response.addresses){
    item = {
      title: response.addresses[i].address,
      subtitle: response.addresses[i].locality,
      buttons: [{
        type:"postback",
        title: "Select",
        payload: parseInt(i)+1
      }]
    };
    items.push(item);
  }

  items.push({
    title: "Want to add new address?",
    buttons:[{
      type: "postback",
      title: "Add address",
      payload: "add"
    }]
  });
  if(response.message) sendTextMessage(response.senderId, response.message);
  sendTextMessage(response.senderId, messages.selectAddress);
  sendGenericMessage(response.senderId,items);
}

function sendArray(response){
  var message = "";
  array = response.array;
  if(response.message){
    message += response.message + '\n';
  }
  message += "Select" + '\n';
  var j;
  for(var i in array){
    j = parseInt(i)+1;
    message += j+")"+ array[i] + '\n';
  }
  sendTextMessage(response.senderId,message);
}

function sendCart(response){
  var items = response.cart.order_items;
  if(items.length == 0){
    sendTextMessage(response.senderId,messages.cartEmpty);
    return;
  }
  var message = "Cart: "+'\n';
  for(var i in items){
    var j = parseInt(i)+1;
    message += items[i].quantity+"x    " + items[i].name.toProperCase() + "\n";
  }
  message += "-----------------------------------------" + '\n'
  message += "Subtotal: Rs." + response.cart.amount + '\n';
  if(response.messageExtra) sendTextMessage(response.senderId,response.messageExtra);
  var buttonsData = []
  buttonsData.push({
    title: "Checkout",
    payload: "checkout"
  });
  buttonsData.push({
    title: "Clear",
    payload: "clear"
  });
  setTimeout(sendButtonTemplate,500,response.senderId,message,buttonsData);

}

function sendConfirmationReceipt(response){
  var items = response.cart.order_items;
  var message = messages.showOrder +'\n';
  for(var i in items){
    var j = parseInt(i)+1;
    message += items[i].quantity+"x    " + items[i].name.toProperCase() + "\n";
  }
  message += "-----------------------------------------" + '\n'
  message += "Subtotal: Rs." + response.price + '\n';
  if(response.tax) message += "Taxes: Rs." + response.tax + '\n';
  else message += "Taxes: Rs.0" +'\n';
  message += "-----------------------------------------" + '\n';
  message += "Total amount: Rs." + response.total +'\n';
  message += "Payment mode: Cash on Delivery" + '\n';
  // console.log(prettyjson.render(response));
  sendTextMessage(response.senderId,message);
  setTimeout(sendConfirmationButtonTemplate,500,response.senderId,messages.generateTipsMessage(['codOnlyTip']),"Place order","Add more items");
}

function sendOrderReceipt(response){
  sendReceiptMessage(response.senderId,response.orderId,response.orderTime,response.address,response.locality,items,response.price,response.tax,response.total);
}


function sendTextMessage(senderId, text) {
  var messageData = {};
  messageData.text = text;
  sendRequest(senderId,messageData);
}

function sendConfirmationButtonTemplate(senderId,title,yesMessage,noMessage){
  var buttonsData = [];
  buttonsData.push({
    type:"postback",
    title: yesMessage,
    payload: "yes"
  });
  buttonsData.push({
    type:"postback",
    title: noMessage,
    payload: "no"
  });
  sendButtonTemplate(senderId,title,buttonsData);
}
　　　
function sendButtonTemplate(senderId,title,buttonsData){

  var buttons = [];
  var button;

  for(var i in buttonsData){
    button = {
      type: "postback",
      title: buttonsData[i].title,
      payload: buttonsData[i].payload
    };
    buttons.push(button);
  }

  var messageData = {
    attachment:{
      type: "template",
      payload: {
        template_type: "button",
        text: title,
        buttons: buttons
      }
    }
  };

  sendRequest(senderId,messageData);
}

function sendGenericMessage(senderId, items) {

  var elements = [];
  var element;

  for (var i in items){
    element = {
      title: items[i].title
    };
    if (items[i].subtitle) element.subtitle = items[i].subtitle;
    if (items[i].image_url) element.image_url = items[i].image_url;
    element.buttons = items[i].buttons;
    elements.push(element);
  };

  var messageData = {};
  messageData = {
    "attachment": {
      "type": "template",
      "payload": {
        "template_type": "generic",
        "elements": elements
      }
    }
  };

  sendRequest(senderId,messageData);

}

function sendReceiptMessage(senderId,orderId,orderTime,address,locality,items,price,tax,total){
  var elements = [];
  var element;

  for (var i in items){
    element = {
      title: items[i].title,
      quantity:items[i].quantity,
      price: items[i].price,
      currency:"INR"
    };
    if (items[i].subtitle) element.subtitle = items[i].subtitle;
    if (items[i].image_url) element.image_url = items[i].image_url;
    elements.push(element);
  };

  var messageData = {
    "attachment":{
      "type":"template",
      "payload":{
        "template_type":"receipt",
        "recipient_name":senderId,
        "order_number":orderId,
        "currency":"INR",
        "payment_method":"COD",    
        "timestamp":orderTime, 
        "elements":elements,
        "address":{
          "street_1":address,
          "city":locality,
          "state":"abc",
          "postal_code": "1234",
          "country": "abc"
        },
        "summary":{
          "subtotal":price,
          "total_tax":tax,
          "total_cost":total
        }      
      }
    }
  }
}


function sendRequest(senderId,messageData){
  request({
    url: 'https://graph.facebook.com/v2.6/me/messages',
    qs: {access_token:token},
    method: 'POST',
    json: {
      recipient: {id:senderId},
      message: messageData,
    }
  }, function(error, response, body) {
    if (error) {
      console.log('Error sending message: ', error);
    } else if (response.body.error) {
      console.log('Error: ', response.body.error);
    }
  });
}

module.exports = sendResponse ;