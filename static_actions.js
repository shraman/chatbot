var redis = require('redis');
var redisClient = redis.createClient();

var state_based_actions = require('./state_based_actions.js');
var message_sender = require('./message_sender');
var messages = require('./messages.js');

//message json has message, sender_id and platform
var execute = function(message_json){
  var message = message_json.message;
  var sender_id = message_json.sender_id;

  switch(message){
    case "help":
    case "man":
      message_sender.sendTextMessage(messages.help_message,message_json);
      break;
    case "restart":
    case "R":
      restart(message_json);
      break;
    case "logout":
      logout(message_json);
      break;
    default:
      default_action(message_json);
  }

}

function initiate_order(message_json,session_data){
  redisClient.hset(message_json.sender_id, 'state', 'WELCOME');
  session_data = { state: 'WELCOME'};
  state_based_actions.execute(message_json,session_data);
}

function logout(message_json){
  //delete session 
  redisClient.del(message_json.sender_id);
  message_sender.sendTextMessage(messages.logout_message,message_json);  
}

function restart(message_json){
  //restart
  logout(message_json);
  message_json.message = "Hi";
  initiate_order(message_json);
}

function default_action(message_json){
  redisClient.hgetall(message_json.sender_id, function(err, session_data){
    if (session_data !== null){ //session exists
      //pass to state_based_action
      state_based_actions.execute(message_json,session_data);
    } else if(message_json.message === "Hi"){ // no session and Hi
      //initiate order
      initiate_order(message_json,session_data);
    } else { // no session and random message
      //show help message
      message_sender.sendTextMessage(messages.help_message,message_json);
    }
  });

}


var static_actions = {
  execute: execute
}

module.exports = static_actions;