var redis = require('redis');
var redisClient = redis.createClient();

var message_sender = require('./message_sender');
var messages = require('./messages.js');

var execute = function(message_json,session_data){
  switch(session_data.state){
    case 'WELCOME':
      welcome(message_json,session_data);
      break;
    case 'ENTER_MOBILE_NUMBER':
      enter_mobile_number(message_json,session_data);
      break;
    case 'ENTER_OTP':
      enter_otp(message_json,session_data);
      break;
    case 'SELECT_ADDRESS':
      select_address(message_json,session_data);
      break;
    case 'SELECT_LOCALITY':
      select_locality(message_json,session_data);
      break;
    case 'ENTER_ADDRESS':
      enter_address(message_json,session_data);
      break;
    case 'SELECT_RESTAURANT':
      select_restaurant(message_json,session_data);
      break;
    case 'ORDER':
      order(message_json,session_data);
      break;
    case 'CHECKOUT':
      checkout(message_json,session_data);
      break;
  }
} 

function welcome(message_json,session_data){
  message_sender.sendTextMessage(messages.welcome_message, message_json);
  redisClient.hset(message_json.sender_id, 'state', 'ENTER_MOBILE_NUMBER');
}

function enter_mobile_number(message_json,session_data){
  //check if entered number is valid
  if(){ //if valid
    //send request for OTP to tinyowl server
    //store OTP
    redisClient.hset(message_json.sender_id,'state','ENTER_OTP');
    message_sender.sendTextMessage(messages.enter_otp,message_json);
  }
  else{ // number invalid
    message_sender.sendTextMessage(messages.mobile_number_invalid,message_json);
  }
}

function enter_otp(message_json,session_data){
  if(){// OTP valid
    //fetch addresses from tinyowl server
    if(){ //address found
      message_sender.sendTextMessage(messages.select_address,message_json);
      // send list of addresses
      redisClient.hset(message_json.sender_id,'state','SELECT_ADDRESS');
    }
    else{ // No address found
      //fetch localities
      message_sender.sendTextMessage(messages.select_locality,message_json);
      //send locality cards
      redisClient.hset(message_json.sender_id,'state','SELECT_LOCALITY');
    }
  }
  else{ // OTP invalid
    message_sender.sendTextMessage(messages.invalid_otp,message_json);
  }
}

function select_address(message_json,session_data){
  if(){// user chooses address
    // show chosen address
    // fetch restaurants
    message_sender.sendTextMessage(messages.select_restaurant,message_json);
    // show restaurant cards
  } else if(){ // user chooses add new address
    //fetch localities
    message_sender.sendTextMessage(messages.select_locality,message_json);
    // show localities
    redisClient.hset(message_json.sender_id,'state','SELECT_LOCALITY');    
  } else{ // user types anything show error
    message_sender.sendTextMessage(messages.select_address_from_above,message_json);
  }
}

function select_locality(message_json,session_data){
  if(){ // input valid
    //show selected locality
    message_sender.sendTextMessage(messages.enter_address,message_json);
    redisClient.hset(message_json.sender_id,'state','ENTER_ADDRESS');
  }
  else{ // not valid
    message_sender.sendTextMessage(messages.invalid_locality,message_json);
  }
}

function enter_address(message_json,session_data){
  
}

function select_restaurant(message_json,session_data){
  
}

function order(message_json,session_data){
  
}

function checkout(message_json,session_data){
  
}

var state_based_actions = {
  execute: execute
}

module.exports = state_based_actions;