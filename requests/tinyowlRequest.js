var config = require('../config.js').settings;
var rp = require('request-promise');


function request(method, url, body){
  body["app_version"] = "4.0.0",
  body["device_id"] = "Chrome",
  body["platform"] = "ANDROID"

  console.log(JSON.stringify(body));
  console.log(url);

  var options = {
    method: method,
    uri: config.host + url,
    body: body,
    json: true // Automatically stringifies the body to JSON
  };

  return rp(options);
}

// POST /login with number
function loginWithNumber(number){
  var body = {
    contact_number: number
  }

  return request('POST', '/user/api/v1/login_with_sms/existing_user/', body);
}

function registerNewUser(name,emailId,phoneNumber){
  var body = {
    email: emailId,
    name: name,
    phone_number: phoneNumber
  }

  return request('POST', '/user/api/v1/login_with_sms/', body);
}

// POST /verify with otp
function verifyWithOtp(preToken, otp){
  var body = {
    post_token: otp,
    pre_token: preToken
  }

  return request('POST', '/user/api/v1/login_with_sms/verify', body);
}

// POST /address
function addAddress(sessionToken,address,localityId,localityName){
  var body = {
    address_details: address,
    landmark: "",
    locality_id: localityId,
    locality_name: localityName,
    session_token: sessionToken
  }

  return request('POST', '/user/api/v1/addresses',body);
}

function getRestaurants(localityId){
  var body = {
    filter_by:{
      cuisines:[]
    },
    token:{
      start_index: 0,
      more: true,
      valid_until:0
    },
    locality_id: localityId
  }

  return request('POST', '/restaurant/api/v1/restaurants/paginate',body);
}

function getItems(nlpUrl){
  var body = {};

  return request('GET','/api/v1/web/restaurant/temp/'+nlpUrl,body);
}

function getSubLocalityId(placeId){
  var body = {};

  return request('GET','/api/v1/web/locality/get_nl_url_data?place_id='+placeId,body);
}

function resendSms(preToken){
  var body = {
    pre_token: preToken 
  }
  return request('POST', '/user/api/v1/login_with_sms/resend',body);
}

function getAddresses(sessionToken){
  return request('GET', '/user/api/v1/addresses?session_token='+sessionToken,{});
}



// POST /invoice
function generateInvoice(placeId, sessionToken, addressId, restaurantId, restaurantName, items){
  var cart = {};
  cart.amount = items.amount;
  cart.number_of_items = items.quantity;
  cart.order_items = [];
  var order_items = items.order_items;
  for(var i in order_items){
    cart.order_items.push({
      item_id: order_items[i].item_id,
      name: order_items[i].name,
      quantity: order_items[i].quantity,
      base_price: order_items[i].price,
      total_price: order_items[i].price,
    });
  }
  var body = {
    order: {
      number_of_items: 1,
      address_id: addressId,
      restaurant_id: restaurantId,
      restaurant_name: restaurantName,
      delivery_type: "DEFAULT",
      offers_by_tinyowl: 0,
      offers_by_restaurant: 0,
      cart: cart
    },
    session_token: sessionToken,
    place_id: placeId
  }

  // console.log(JSON.stringify(body));

  return request('POST', '/api/v1/orders/invoice', body);
}

// POST /order
function placeOrder(orderId, sessionToken, total, restaurantId, items){
  var cart = {
    amount: items.amount,
    number_of_items: items.quantity,
    order_items:[]
  };
  var order_items = items.order_items;
  for(var i in order_items){
    cart.order_items.push({
      item_id: order_items[i].item_id,
      item_size_id: order_items[i].item_size_id,
      name: order_items[i].name,
      quantity: order_items[i].quantity,
      base_price: order_items[i].price,
      total_price: order_items[i].price,
    });
  }

  var body = {
    order_id: orderId,
    order: {
      payable_amount: total,
      paid_by_cod: total,
      restaurant_id: restaurantId,
      cart: cart
    },
    payment: {
      method: "COD",
      sdk: "TINYOWL_DEFAULT",
      bring_change: null
    },
    session_token: sessionToken
  }
   

  return request('POST', '/api/v1/orders/' + orderId + '/place', body);
}

var tinyowlRequest = {
  getItems: getItems,
  loginWithNumber: loginWithNumber,
  verifyWithOtp: verifyWithOtp,
  generateInvoice: generateInvoice,
  placeOrder: placeOrder,
  addAddress: addAddress,
  getRestaurants: getRestaurants,
  getSubLocalityId: getSubLocalityId,
  resendSms: resendSms,
  registerNewUser: registerNewUser,
  getAddresses: getAddresses
}

module.exports = tinyowlRequest;