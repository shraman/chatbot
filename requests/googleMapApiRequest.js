var config = require('../config.js').settings;
var rp = require('request-promise');

function request(body){

  var options = {
    method: "GET",
    uri: config.googleMapApiUrl,
    qs: body,
    json: true // Automatically stringifies the body to JSON
  };

  return rp(options);
}


function search(keyWord){
  var body = {
    input: keyWord,
    components: "country:IN",
    key: config.googleMapApiKey
  }
  return request(body);
}


module.exports = {
  search: search
}