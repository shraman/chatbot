var messages = {
  logoutMessage: 'You have successfully logged out. Say "Hi" to start your order again!',
  helpMessage: 'To start your order say Hi\nTips:\n- Type "restart" or "re" anytime to restart your order\n- Type "logout" or "lo" anytime to logout',
  welcomeMessage: ' Hey there, I’m runnr. I do blazing fast food delivery.🍕🍔🍦 Tell us your mobile number to get started!',
  mobileNumberInvalid: 'Wait, that doesn’t look like a correct number! 👊 please re-enter your 10-digit number',
  mobileNumberNotRegistered: 'Looks like it’s your first time here! Let’s get to know each other a little first. What’s your name?',
  enterOtp: '4 magic digits will log you in. Tell me the OTP sent to your number',
  enterName: 'Looks like it’s your first time here! Let’s get to know each other a little first. What’s your name?',
  enterEmailId: 'and your email ID? (No spam mails, I promise) 😛',
  invalidOtp: 'Hey, no cheating! 😑 Check the OTP and enter again',
  invalidEmail: 'That doesn’t look right! 😒 Enter your email ID again',
  selectAddress: 'Where would you like us to deliver your food?',
  selectLocality: 'Please select your locality',
  selectAddressFromAbove: 'Sorry, I have limited understanding of humans for i am only a chat bot. Please choose from the above',
  selectRestaurantFromAbove: 'Sorry, I have limited understanding of humans for i am only a chat bot. Please choose from the above',
  selectRestaurant: 'Where would you like to order from?',
  selectedAddress: 'Selected Address',
  tryOtherSearch: 'We didn\'t find any matching items sir. You have exquisite taste, Try searching for something else' ,
  showingMatchedItems: 'Matching items',
  orderSuccessful: 'Thank you, we’ve placed your order. Keep the change ready',
  enterAddress: 'Tell me the delivery address in a single line. I am only an ordering bot',
  enterLocality: 'Help us by telling your building/ society/ street name',
  selectLocalityFromAbove: 'Sorry, I have limited understanding of humans for i am only a chat bot. Please choose from the above',
  confirmAddress: 'Are you sure this is the right address?',
  reenterAddress: 'To err is human. Please tell me your address again',
  showOrder: 'Order Summary:',
  placeOrderInvalid: 'Sorry, I have limited understanding of humans for i am only a chat bot. Please choose from the above',
  noMatchingLocalities: ' I couldn’t find your locality. Try a different building/ society/ street name',
  locationOutOfRange: 'Location out of range',
  notActiveLocality: 'We haven’t launched in your locality yet. Sorry about that',
  smsSentAgain: 'Okay, there you go. We sent you the OTP again!',
  cartEmpty: 'Damn, you have no items in your cart!',
  selectAnotherAddress: 'Sorry, no restaurants open in your area\nPlease select another address',
  noRestaurantOpen: 'Looks like all the restaurants are closed',
  apiError: "Sorry mate, bad time. Our server is down :/",

  //Tips
  restartTip: 'You can send me a “hello” to start over anytime',
  checkoutTip: 'Type "checkout" or "co" to freeze cart and proceed to payment',
  clearTip: 'Type "clear" or "c" to clear your cart',
  confirmTip: 'Type "yes" or "y" to confirm , "no" or "n" to go back',
  confirmOrderTip: 'Type "yes" or "y" to place your order, "no" or "n" to go back',
  codOnlyTip: 'Only COD payment is available, we’ll add other payment options soon',
  searchTip: ' If you are searching for something specific, please type it. (Example : "biryani”, “pizza”, etc.)',
  showCartTip: 'Type "show" or "s" to view your cart',
  searchLocalityTip: ' Can’t find your locality? Try a different building/ society/ street name',
  addMoreTip: 'You can add/search for more items'
}

messages.searchingItems = function(itemName){
  return "Searching for "+itemName+" ...";
}

messages.showItemAdded = function(itemName){
  return "Added "+itemName+" to your cart";
}

messages.showItemRemoved = function(itemName){
  return "Removed "+itemName+" from your cart";
}

messages.whereIn = function(locality_name){
  return 'I need you to be a little more specific. Where in '+locality_name+'?';
}

messages.showingTopItems = function(restaurant){
  return 'Showing best 10 items of ' + restaurant;
}

messages.generateTipsMessage = function(tips){
  var message = "Tips💡\n";
  var j;
  for(var i in tips){
    j = parseInt(i) + 1;
    message +=  "- " + messages[tips[i]]+ "\n";
  }
  return message;
}

module.exports = messages;