var messages = require('./staticMessages/genericMessages.js');

function staticResponse(messageJson){
  var response = { 
    senderId: messageJson.senderId,
    platform: messageJson.platform, 
    type: 'PLAIN_TEXT'
  }
  switch(messageJson.message){
    case "help":
    case "man":
      response.message = messages.generateTipsMessage(['restartTip']);
      break;
    default:
      response = null
  }
  return response;
}

module.exports = staticResponse;