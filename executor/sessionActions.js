var bluebird = require('bluebird');
var redis = require('redis');
bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);
var redisClient = redis.createClient();

var messages = require('./staticMessages/genericMessages.js');

function logout(messageJson,sessionData){
  var response = { 
    senderId: messageJson.senderId,
    platform: messageJson.platform,
    type: "PLAIN_TEXT"
  }
  redisClient.del(messageJson.senderId);
  response.message = messages.logoutMessage
  return response;
}

function restart(messageJson,sessionData){
  var response = { 
    senderId: messageJson.senderId,
    platform: messageJson.platform,
    type: "PLAIN_TEXT"
  }

  //user has valid session
  if(sessionData.mobile_number && sessionData.pre_token && sessionData.session_token && sessionData.addresses){
    redisClient.delAsync(messageJson.senderId).then(function(){
      redisClient.hset(messageJson.senderId,'mobile_number',sessionData.mobile_number);
      redisClient.hset(messageJson.senderId,'pre_token',sessionData.pre_token);
      redisClient.hset(messageJson.senderId,'session_token',sessionData.session_token);
      redisClient.hset(messageJson.senderId,'addresses',sessionData.addresses);
    });
    var addresses = JSON.parse(sessionData.addresses);
    if(addresses[0]){ //address found
      response.type = "ADDRESSES";
      response.addresses = [];
      for(var i in addresses){
        response.addresses.push({
          address: addresses[i].address_details,
          locality: addresses[i].locality_name
        });
        if(i == 8) break;
      };
      redisClient.hset(messageJson.senderId,'state','SELECT_ADDRESS');
    }
    else{ // No address found
      redisClient.hset(messageJson.senderId,'state','SELECT_LOCALITY');      
      response.message = messages.enterLocality;
    }
    return response;
  }

  //start new session
  redisClient.delAsync(messageJson.senderId).then(function(){
    redisClient.hset(messageJson.senderId,'state','ENTER_MOBILE_NUMBER');    
  });
  response.message = messages.welcomeMessage;
  response.messageExtra = messages.generateTipsMessage(['restartTip']);
  return response;
}

var sessionActions = {
  restart: restart,
  logout: logout
}

module.exports = sessionActions;