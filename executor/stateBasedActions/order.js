// ----------------------- HEADERS ------------------
// Common to all
// TODO: abstract out all common require commands in state actions
var bluebird = require('bluebird');
var redis = require('redis');
bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);
var redisClient = redis.createClient();

var messages = require('../staticMessages/genericMessages.js');
var proxy = require('../../tinyowlProxy.js');
var tinyowlRequest = require('../../requests/tinyowlRequest.js');

// -----------------------

//TODO: HACK, added for showing current item added to cart, change updateCart accordingly
var currentItem;

String.prototype.toProperCase = function() {
  return this.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
};


function order(messageJson,sessionData){
  var response = {
    senderId: messageJson.senderId,
    platform: messageJson.platform,
    type: "PLAIN_TEXT"
  };
  var message = messageJson.message;
  switch (true){
    case /^add [0-9]{1,2}$/.test(message): // add specified item to cart
      response.type = "CART";
      var itemNumber = parseInt(message.split(' ')[1]) - 1;  
      var cart = updateCart(sessionData,itemNumber,"ADD");
      response.cart = cart;
      // response.message = messages.showItemAdded(currentItem.name.toProperCase());
      // response.messageExtra = messages.generateTipsMessage(['checkoutTip','clearTip']);
      if(!sessionData.add_more_tip_shown) response.messageExtra = messages.generateTipsMessage(['addMoreTip']);
      redisClient.hset(messageJson.senderId,'cart',JSON.stringify(cart));
      redisClient.hset(messageJson.senderId,'add_more_tip_shown',"true");
      return response;
      break;
    case /^rem [0-9]{1,2}$/.test(message):  // remove specified item from cart
      response.type = "CART";
      var itemNumber = parseInt(message.split(' ')[1]) - 1;  
      var cart = updateCart(sessionData,itemNumber,"REMOVE");
      // response.message = messages.showItemRemoved(currentItem.name.toProperCase());
      // response.messageExtra = messages.generateTipsMessage(['checkoutTip','clearTip']);
      if(!sessionData.add_more_tip_shown) response.messageExtra = messages.generateTipsMessage(['addMoreTip']);
      response.cart = cart;
      redisClient.hset(messageJson.senderId,'cart',JSON.stringify(cart));
      return response;
      break;
    // case /^show$/.test(message):
    // case /^s$/.test(message):
    //   var cart = JSON.parse(sessionData.cart);
    //   if (cart.quantity == 0) {
    //     response.message = messages.cartEmpty;
    //     return response;
    //   }
    //   response.type = "CART";
    //   response.cart = cart;
    //   return response;
    //   break;
    case /^checkout$/.test(message): // proceed to checkout
    case /^co$/.test(message):
      var cart = JSON.parse(sessionData.cart);
      if (cart.quantity == 0) {
        response.message = messages.cartEmpty;
        return response;
      }
      response.type = "CONFIRMATION_RECEIPT";
      var responsePromise = tinyowlRequest.generateInvoice(sessionData.locality_id,sessionData.session_token,sessionData.address_id,sessionData.restaurant_id,sessionData.restaurant_name,cart).then(function(res){
        var parsedBody = res.payload;
        console.log("---response---");
        console.log(JSON.stringify(res));
        response.cart = JSON.parse(sessionData.cart);
        // response.price = parsedBody.invoice[0].value;
        // response.tax = parsedBody.invoice[1].value;
        var invoice = parsedBody.invoice;
        for(var i in invoice){
          switch(invoice[i].key){
            case "cart_amount":
              response.price = invoice[i].value;
              break;
            case "service_tax":
              response.tax = invoice[i].value;
              break;
          }
        }
        response.total = parsedBody.payable_amount;
        redisClient.hset(messageJson.senderId,'order_id',parsedBody.order_id);
        redisClient.hset(messageJson.senderId,'total',parsedBody.payable_amount);
        redisClient.hset(messageJson.senderId,'state','CHECKOUT');
        return response;
      });
      return responsePromise;
      break;
    case /^clear$/.test(message): // clear cart
    case /^c$/.test(message):
      response.type = "CART"
      var cart = getEmptyCart();
      response.cart = cart;
      redisClient.hset(messageJson.senderId,'cart',JSON.stringify(cart));
      return response;
      break;
    default: // search for items with this string and show results
      message = message.toLowerCase();
      var items = JSON.parse(sessionData.items);
      var matchedItems = itemSearch(items, message);
      var itemSizes = JSON.parse(sessionData.item_sizes);

      var sizeDetails;
      response.type = "ITEMS";
      response.items = [];
      for (var i in matchedItems){
        sizeDetails = findSizeDetails(matchedItems[i],itemSizes);
        response.items.push({
          name: matchedItems[i].name,
          image_url: matchedItems[i].image_url,
          veg_type: matchedItems[i].veg_type,
          description: matchedItems[i].description,
          price: sizeDetails.price,
          size: sizeDetails.size
        });
        if(i == 9) break;
      }

      if(response.items.length === 0){
        response.type = "PLAIN_TEXT";
        response.message = messages.tryOtherSearch;
      } else {
        response.message = messages.searchingItems(message);
      }
      redisClient.hset(messageJson.senderId,'item_search_results',JSON.stringify(matchedItems));
      return response;
  }
}

function itemSearch(items, searchKey){
  var results = [];
  var match;
  var tokens = searchKey.split(' ');
  for(var i in items){
    match = true;
    for(var j in tokens){
      if(items[i].name.toLowerCase().search(tokens[j])== -1) {
        match = false;
        break;
      }
    }
    if(match) results.push(items[i]);
  }
  return results;
}

function updateCart(sessionData,number,operation){
  var cart;
  var items;

  if(sessionData.cart == null){
    cart = getEmptyCart();
  } else{
    cart = JSON.parse(sessionData.cart);
  }

  if(sessionData.item_search_results && JSON.parse(sessionData.item_search_results).length > 0){ // item from search results
    items = JSON.parse(sessionData.item_search_results)
  } else { // item from top 10
    items = JSON.parse(sessionData.items)
  }

  if(number > items.length-1) return cart;

  var itemSizes = JSON.parse(sessionData.item_sizes);
  var itemToAdd = items[number];
  currentItem = itemToAdd;
  var sizeDetails = findSizeDetails(itemToAdd,itemSizes);
  var price = sizeDetails.price;
  var sizeId = sizeDetails.id;
  // var size = sizeDetails.size;

  if(operation === "ADD"){ // add item to cart
    cart.quantity += 1;
    cart.amount += parseInt(price);
    var index = findItem(itemToAdd.id,cart.order_items);
    if(index){
      cart.order_items[index].quantity += 1;
    } else{
      var newItem = {
        item_id:itemToAdd.id,
        item_size_id:sizeId,
        name:itemToAdd.name,
        quantity:1,
        price:price
      }
      cart.order_items.push(newItem);
    }
    return cart;

  } else{ // remove item from cart
    var index = findItem(itemToAdd.id,cart.order_items);
    if(index){
      cart.quantity -= 1;
      cart.amount -= price;
      var itemFound = cart.order_items.splice(index,1)[0];
      if(itemFound.quantity == 1) return cart;
      else{
        itemFound.quantity -= 1 ;
        cart.order_items.push(itemFound);
        return cart;
      }
    } else{
      return cart;
    }
  }
}

function findItem(itemId, orderItems){
  for(var i in orderItems){
    if(orderItems[i].item_id == itemId) return i;
  }
  return null;
}

function getEmptyCart(){
  var cart = {
    amount:0,
    quantity:0,
    order_items: []
  }
  return cart;
}

function findSizeDetails(item,itemSizes){
  var itemId = item.id;
  var lastResult;
  for(var i in itemSizes){
    if(itemSizes[i].item_id === itemId) lastResult = itemSizes[i];
    if(itemSizes[i].item_id === itemId && (itemSizes[i].size === "NONE" || itemSizes[i].size === "Classic" || itemSizes[i].size === "REGULAR")) return itemSizes[i];
  }
  return lastResult;
}


module.exports = order;