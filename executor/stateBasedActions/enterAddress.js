// ----------------------- HEADERS ------------------
// Common to all
// TODO: abstract out all common require commands in state actions
var bluebird = require('bluebird');
var redis = require('redis');
bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);
var redisClient = redis.createClient();

var messages = require('../staticMessages/genericMessages.js');
var proxy = require('../../tinyowlProxy.js');

// -----------------------

function enterAddress(messageJson,sessionData){
  var response = {
    senderId: messageJson.senderId,
    platform: messageJson.platform,
    type: "ADDRESS_CONFIRMATION"
  };
  var address = messageJson.message;
  redisClient.hset(messageJson.senderId,'temporary_address',address);
  response.message = messages.confirmAddress;
  redisClient.hset(messageJson.senderId,'state','CONFIRM_ADDRESS');
  return response;
}

module.exports = enterAddress;