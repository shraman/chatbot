// ----------------------- HEADERS ------------------
// Common to all
// TODO: abstract out all common require commands in state actions
var bluebird = require('bluebird');
var redis = require('redis');
bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);
var redisClient = redis.createClient();

var messages = require('../staticMessages/genericMessages.js');
var proxy = require('../../tinyowlProxy.js');
var tinyowlRequest = require('../../requests/tinyowlRequest.js');

// -----------------------

function confirmAddress(messageJson,sessionData){
  var response = {
    senderId: messageJson.senderId,
    platform: messageJson.platform,
    type: "PLAIN_TEXT"
  };

  switch(messageJson.message){
    case "yes":
    case "y":
      var responsePromise = tinyowlRequest.addAddress(sessionData.session_token,sessionData.temporary_address,sessionData.locality_id,sessionData.locality_name).then(function(res){
        redisClient.hset(messageJson.senderId,'address_id',res.id);
        return tinyowlRequest.getRestaurants(sessionData.locality_id);
      }).then(function(res){
        response.type = "RESTAURANTS";
        response.message = "Your order will be delivered at " + sessionData.temporary_address;
        var restaurants = res.restaurants;
        if(restaurants == null) restaurants = [];
        redisClient.hset(messageJson.senderId,'restaurants',JSON.stringify(restaurants));
        response.restaurants = [];
        for (var i in restaurants){
          if(restaurants[i].is_open) response.restaurants.push({
            name: restaurants[i].name,
            image_url: restaurants[i].logo_url
          });
        }

        if(response.restaurants.length == 0 ){ // no restaurants in locality
          var innnerPromise = tinyowlRequest.getAddresses(sessionData.session_token).then(function(res){
            var addresses = res.addresses;
            response.type = "ADDRESSES";
            response.message = messages.selectAnotherAddress;  
            response.addresses = [];
            for(var i in addresses){
              response.addresses.push({
                address: addresses[i].address_details,
                locality: addresses[i].locality_name
              });
              if(i == 8) break;
            };
            redisClient.hset(messageJson.senderId,'state','SELECT_ADDRESS');
            redisClient.hset(messageJson.senderId,'addresses',JSON.stringify(addresses));
            return response;
          });
          return innnerPromise;
        }

        redisClient.hset(messageJson.senderId,'state','SELECT_RESTAURANT');
        return response;
      });
      return responsePromise;
      break;
    case "no":
    case "n":
      response.message = messages.reenterAddress;
      redisClient.hset(messageJson.senderId,'state','ENTER_ADDRESS');
      return response;
      break;
    default:
      response.message = messages.confirmAddress;
      return response;
  } 

}

module.exports = confirmAddress;