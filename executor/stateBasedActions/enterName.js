// ----------------------- HEADERS ------------------
// Common to all
// TODO: abstract out all common require commands in state actions
var bluebird = require('bluebird');
var redis = require('redis');
bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);
var redisClient = redis.createClient();

var messages = require('../staticMessages/genericMessages.js');
var proxy = require('../../tinyowlProxy.js');

// -----------------------

function enterName(messageJson,sessionData){
  var response = {
    senderId: messageJson.senderId,
    platform: messageJson.platform,
    type: "PLAIN_TEXT"
  };
  var name = messageJson.message;
  redisClient.hset(messageJson.senderId,'name',name);
  redisClient.hset(messageJson.senderId,'state','ENTER_EMAIL_ID');
  response.message = messages.enterEmailId;
  return response;
}

module.exports = enterName;