// ----------------------- HEADERS ------------------
// Common to all
// TODO: abstract out all common require commands in state actions
var bluebird = require('bluebird');
var redis = require('redis');
bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);
var redisClient = redis.createClient();

var messages = require('../staticMessages/genericMessages.js');
var proxy = require('../../tinyowlProxy.js');

// -----------------------


function welcome(messageJson,sessionData){
  var response = { 
    senderId: messageJson.senderId,
    platform: messageJson.platform,
    type: "PLAIN_TEXT"
  }
  redisClient.delAsync(messageJson.senderId).then(function(){
    redisClient.hset(messageJson.senderId,'state','ENTER_MOBILE_NUMBER');    
  });
  response.message = messages.welcomeMessage;
  response.messageExtra = messages.generateTipsMessage(['restartTip']);
  return response;
}

module.exports = welcome;