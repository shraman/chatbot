// ----------------------- HEADERS ------------------
// Common to all
// TODO: abstract out all common require commands in state actions
var bluebird = require('bluebird');
var redis = require('redis');
bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);
var redisClient = redis.createClient();

var messages = require('../staticMessages/genericMessages.js');
var proxy = require('../../tinyowlProxy.js');
var tinyowlRequest = require('../../requests/tinyowlRequest.js');

// -----------------------

function selectAddress(messageJson,sessionData){
  var response = {
    senderId: messageJson.senderId,
    platform: messageJson.platform,
    type: "PLAIN_TEXT"
  };
  var addressNumber = parseInt(messageJson.message);
  var selectedAddress;

  var promise =  redisClient.hgetAsync(messageJson.senderId,'addresses').then(function(res){
    addresses = JSON.parse(res);
    if(addressNumber && addressNumber <= addresses.length ){// user chooses given address
      //show selected address
      response.message = messages.selectedAddress + '\n';
      response.message += addresses[addressNumber-1].address_details + '\n';
      var localityId = addresses[addressNumber-1].locality_id;
      var addressId = addresses[addressNumber-1].id;
      var address = addresses[addressNumber-1].address_details;
      selectedAddress = address;
      redisClient.hset(messageJson.senderId,'locality_id',localityId);
      redisClient.hset(messageJson.senderId,'address_id',addressId);
      redisClient.hset(messageJson.senderId,'address',address);
      return ["SELECTED_ADDRESS",tinyowlRequest.getRestaurants(localityId)];

    } else if(messageJson.message == "add"){ // user chooses add new address
      return ["ADD_ADDRESS",null];
    } else{ // user types anything show error
      response.message = messages.selectAddressFromAbove;
      return ["INVALID_INPUT",null];
    }
  }).spread(function(input,res){
    if(input === "SELECTED_ADDRESS"){
      var restaurants = res.restaurants;
      response.type = "RESTAURANTS";
      redisClient.hset(messageJson.senderId,'restaurants',JSON.stringify(restaurants));
      redisClient.hset(messageJson.senderId,'state','SELECT_RESTAURANT');      
      response.restaurants = [];
      response.message = "Your order will be delivered at " + selectedAddress;
      for (var i in restaurants){
        if(restaurants[i].is_open) response.restaurants.push({
          name: restaurants[i].name,
          image_url: restaurants[i].logo_url
        });
      }

      console.log(response.restaurants);

      if(response.restaurants.length == 0){ // no restaurants open
        var addresses = JSON.parse(sessionData.addresses);
        response.type = "ADDRESSES";
        response.messageAbove = messages.noRestaurantOpen;
        response.message = messages.selectAnotherAddress;
        response.addresses = [];
        for(var i in addresses){
          response.addresses.push({
            address: addresses[i].address_details,
            locality: addresses[i].locality_name
          });
          if(i == 8) break;
        };
        redisClient.hset(messageJson.senderId,'state','SELECT_ADDRESS');
        return response;
      }
      return response;
    } else if(input === "ADD_ADDRESS"){
      var localities = JSON.parse(res);
      redisClient.hset(messageJson.senderId,'state','SELECT_LOCALITY');      
      response.message = messages.enterLocality;
      return response;
    } else{
      return response;
    }
  });

  return promise;
}

module.exports = selectAddress;