// ----------------------- HEADERS ------------------
// Common to all
// TODO: abstract out all common require commands in state actions
var bluebird = require('bluebird');
var redis = require('redis');
bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);
var redisClient = redis.createClient();

var messages = require('../staticMessages/genericMessages.js');
var proxy = require('../../tinyowlProxy.js');
var tinyowlRequest = require('../../requests/tinyowlRequest.js');

// -----------------------

function selectRestaurant(messageJson,sessionData){
  var response = {
    senderId: messageJson.senderId,
    platform: messageJson.platform,
    type: "PLAIN_TEXT"
  };
  var restaurantNumber = parseInt(messageJson.message);
  var selectedRestaurant;

  var promise =  redisClient.hgetAsync(messageJson.senderId,'restaurants').then(function(res){
    restaurants = JSON.parse(res);
    if(restaurantNumber && restaurantNumber <= restaurants.length ){// user chooses given address
      var restaurantNlpUrl = restaurants[restaurantNumber-1].nlp_url;
      var restaurantId = restaurants[restaurantNumber-1].id;
      var restaurantName = restaurants[restaurantNumber-1].name;
      selectedRestaurant = restaurantName;
      redisClient.hset(messageJson.senderId, 'restaurant_id', restaurantId);
      redisClient.hset(messageJson.senderId, 'restaurant_name', restaurantName);
      return ["SELECTED_RESTAURANT",tinyowlRequest.getItems(restaurantNlpUrl)];  
      // return ["SELECTED_RESTAURANT",proxy.getDishes(restaurantNlpUrl)];  
    } else{ // user types anything show error
      response.message = messages.selectRestaurantFromAbove;
      return ["INVALID_INPUT",null]
    }
  }).spread(function(input,res){
    if(input === "SELECTED_RESTAURANT"){
      var items = res.payload.menu.items;
      var itemSizes = res.payload.menu.item_sizes;
      redisClient.hset(messageJson.senderId,'items',JSON.stringify(items));
      redisClient.hset(messageJson.senderId,'item_sizes',JSON.stringify(itemSizes));
      redisClient.hset(messageJson.senderId,'state','ORDER');  
      response.type = "ITEMS";
      response.message = messages.showingTopItems(selectedRestaurant);   
      response.messageBelow = messages.generateTipsMessage(['searchTip']); 
      response.items = [];
      for (var i in items){
        response.items.push({
          name: items[i].name,
          image_url: items[i].image_url,
          veg_type: items[i].veg_type,
          description: items[i].description,
          price: findPrice(items[i],itemSizes),
          size: findSizeDetails(items[i],itemSizes).size
        });
        if(i == 9) break;
      }
      return response;
    } else{
      return response;
    }
  });

  return promise;
}


function findPrice(item,itemSizes){
  var itemId = item.id;
  var lastResultPrice;
  for(var i in itemSizes){
    if(itemSizes[i].item_id === itemId) lastResultPrice = itemSizes[i].price;
    if(itemSizes[i].item_id === itemId && (itemSizes[i].size === "NONE" || itemSizes[i].size === "Classic" || itemSizes[i].size === "REGULAR")) return itemSizes[i].price;
  }
  return lastResultPrice;
}

function findSizeDetails(item,itemSizes){
  var itemId = item.id;
  var lastResult;
  for(var i in itemSizes){
    if(itemSizes[i].item_id === itemId) lastResult = itemSizes[i];
    if(itemSizes[i].item_id === itemId && (itemSizes[i].size === "NONE" || itemSizes[i].size === "Classic" || itemSizes[i].size === "REGULAR")) return itemSizes[i];
  }
  return lastResult;
}




module.exports = selectRestaurant;