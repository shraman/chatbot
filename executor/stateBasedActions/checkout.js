// ----------------------- HEADERS ------------------
// Common to all
// TODO: abstract out all common require commands in state actions
var bluebird = require('bluebird');
var redis = require('redis');
bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);
var redisClient = redis.createClient();

var messages = require('../staticMessages/genericMessages.js');
var proxy = require('../../tinyowlProxy.js');
var tinyowlRequest = require('../../requests/tinyowlRequest.js');

// -----------------------

function checkout(messageJson,sessionData){
  var response = {
    senderId: messageJson.senderId,
    platform: messageJson.platform,
    type: "PLAIN_TEXT"
  };
  if(messageJson.message === "yes" || messageJson.message === "y"){
    var cart = JSON.parse(sessionData.cart);
    var responsePromise = tinyowlRequest.placeOrder(sessionData.order_id,sessionData.session_token,sessionData.total,sessionData.restaurant_id,cart).then(function(res){
      response.message = messages.orderSuccessful + '\n';
      response.message += "Order id :" + res.order_id +  '\n';
      response.message += "Total amount :" + sessionData.total +  '\n';
      response.message += "Payment mode: Cash on Delivery";

      redisClient.delAsync(messageJson.senderId);
      redisClient.hset(messageJson.senderId,'state','WELCOME');
      return response;
    });
    return responsePromise;
  } else if(messageJson.message === "no" || messageJson.message === "n"){
    response.type = "ITEMS";
    redisClient.hset(messageJson.senderId,'state','ORDER');
    redisClient.hset(messageJson.senderId,'item_search_results','[]');
    var items = JSON.parse(sessionData.items);
    var itemSizes = JSON.parse(sessionData.item_sizes);
    response.message = messages.showingTopItems(sessionData.restaurant_name);   
    response.messageBelow = messages.generateTipsMessage(['searchTip']); 
    response.items = [];
    for (var i in items){
      response.items.push({
        name: items[i].name,
        image_url: items[i].image_url,
        veg_type: items[i].veg_type,
        description: items[i].description,
        price: findSizeDetails(items[i],itemSizes).price
      });
      if(i == 9) break;
    }
    return response;
  } else {
    response.message = messages.placeOrderInvalid;
    return response;
  }
}

function findSizeDetails(item,itemSizes){
  var itemId = item.id;
  var lastResult;
  for(var i in itemSizes){
    if(itemSizes[i].item_id === itemId) lastResult = itemSizes[i];
    if(itemSizes[i].item_id === itemId && (itemSizes[i].size === "NONE" || itemSizes[i].size === "Classic" || itemSizes[i].size === "REGULAR")) return itemSizes[i];
  }
  return lastResult;
}

module.exports = checkout;
