// ----------------------- HEADERS ------------------
// Common to all
// TODO: abstract out all common require commands in state actions
var bluebird = require('bluebird');
var redis = require('redis');
bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);
var redisClient = redis.createClient();

var messages = require('../staticMessages/genericMessages.js');
var proxy = require('../../tinyowlProxy.js');
var tinyowlRequest = require('../../requests/tinyowlRequest.js');
var googleMapApiRequest = require('../../requests/googleMapApiRequest.js');


// -----------------------

function selectLocality(messageJson,sessionData){ 
  var response = {
    senderId: messageJson.senderId,
    platform: messageJson.platform,
    type: "PLAIN_TEXT"
  };

  var message = messageJson.message;

  if(/^[0-9]*$/.test(message)){ // selected a locality
    var localityNumber = parseInt(message);
    var localities = JSON.parse(sessionData.localities);
    if (localityNumber <= localities.length){ // in range
      if(localities[localityNumber-1].types[0] == 'sublocality_level_1'){ // Big locality
        response.message = messages.whereIn(localities[localityNumber-1].terms[0].value);
        return response;
      }
      var responsePromise = tinyowlRequest.getSubLocalityId(localities[localityNumber-1].place_id).then(function(res){
        if(res.payload == null){ // not active in this area
          response.message = messages.notActiveLocality;
          return response;
        }
        var subLocalityId = res.payload.locality_codes.sub_locality_id;
        redisClient.hset(messageJson.senderId, 'locality_id', subLocalityId);
        redisClient.hset(messageJson.senderId, 'locality_name', localities[localityNumber-1].description);
        redisClient.hset(messageJson.senderId, 'state', 'ENTER_ADDRESS');
        response.message = messages.enterAddress;
        return response;
      });
      return responsePromise;
    } else{ // out of range
      response.message = messages.locationOutOfRange;
      return response
    }

  } else{ // search for localities using given string
    var responsePromise = googleMapApiRequest.search(message).then(function(res){
      var localities = res.predictions;
      // console.log(localities);
      localities = filterLocalities(localities);
      if(localities.length == 0){ // no results
        response.message = messages.noMatchingLocalities;
        return response;
      }else {
        response.type = "LOCALITIES";
        response.message = messages.selectLocality;
        response.localities = localities;
        redisClient.hset(messageJson.senderId,'localities',JSON.stringify(localities));
        return response;
      }
    });

    return responsePromise;
  }

}

function filterLocalities(localities){
  var filteredLocalities = [];
  var restrictedTypes = ['administrative_area_level_1', 'country', 'locality'];
  for(var i in localities){
    if(restrictedTypes.indexOf(localities[i].types[0]) == -1) filteredLocalities.push(localities[i]);
  }
  return filteredLocalities;
}

module.exports = selectLocality;