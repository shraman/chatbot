var bluebird = require('bluebird');
var redis = require('redis');
bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);
var redisClient = redis.createClient();

var messages = require('../staticMessages/genericMessages.js');
var proxy = require('../../tinyowlProxy.js');

var welcome = require('./welcome.js');
var enterMobileNumber = require('./enterMobileNumber.js');
var enterName = require('./enterName.js');
var enterEmailId = require('./enterEmailId.js');
var enterOtp = require('./enterOtp.js');
var selectAddress = require('./selectAddress.js');
var selectLocality = require('./selectLocality.js');
var enterAddress = require('./enterAddress.js');
var confirmAddress = require('./confirmAddress.js');
var selectRestaurant = require('./selectRestaurant.js');
var order = require('./order.js');
var checkout = require('./checkout.js');

function execute(messageJson,sessionData){
  var response;
  switch(sessionData.state){
    case 'WELCOME':
      response = welcome(messageJson,sessionData);
      break;
    case 'ENTER_MOBILE_NUMBER':
      response = enterMobileNumber(messageJson,sessionData);
      break;
    case 'ENTER_NAME':
      response = enterName(messageJson,sessionData);
      break;
    case 'ENTER_EMAIL_ID':
      response = enterEmailId(messageJson,sessionData);
      break;
    case 'ENTER_OTP':
      response = enterOtp(messageJson,sessionData);
      break;
    case 'SELECT_ADDRESS':
      response = selectAddress(messageJson,sessionData);
      break;
    case 'SELECT_LOCALITY':
      response = selectLocality(messageJson,sessionData);
      break;
    case 'ENTER_ADDRESS':
      response = enterAddress(messageJson,sessionData);
      break;
    case 'CONFIRM_ADDRESS':
      response = confirmAddress(messageJson,sessionData);
      break;
    case 'SELECT_RESTAURANT':
      response = selectRestaurant(messageJson,sessionData);
      break;
    case 'ORDER':
      response = order(messageJson,sessionData);
      break;
    case 'CHECKOUT':
      response = checkout(messageJson,sessionData);
      break;
  }
  return response;
}


module.exports = execute;