// ----------------------- HEADERS ------------------
// Common to all
// TODO: abstract out all common require commands in state actions
var bluebird = require('bluebird');
var redis = require('redis');
bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);
var redisClient = redis.createClient();

var messages = require('../staticMessages/genericMessages.js');
var proxy = require('../../tinyowlProxy.js');
var tinyowlRequest = require('../../requests/tinyowlRequest.js');

// -----------------------

function enterOtp(messageJson,sessionData){
  var response = {
    senderId: messageJson.senderId,
    platform: messageJson.platform,
    type: "PLAIN_TEXT"
  };
  var preToken = sessionData.pre_token;
  if(messageJson.message == "resend"){ // resend sms
    tinyowlRequest.resendSms(preToken);
    response.message = messages.smsSentAgain;
    return response;
  }

  var otp = messageJson.message;
  var responsePromise = tinyowlRequest.verifyWithOtp(preToken,otp).then(function(parsedBody){
    console.log(parsedBody);
    if(parsedBody.profile){// OTP valid
      addresses = parsedBody.profile.addresses
      if(addresses[0]){ //address found
        response.type = "ADDRESSES";
        response.addresses = [];
        for(var i in addresses){
          response.addresses.push({
            address: addresses[i].address_details,
            locality: addresses[i].locality_name
          });
          if(i == 8) break;
        };
        redisClient.hset(messageJson.senderId,'state','SELECT_ADDRESS');
        redisClient.hset(messageJson.senderId,'session_token',parsedBody.session_token);
        redisClient.hset(messageJson.senderId,'addresses',JSON.stringify(addresses));
      }
      else{ // No address found
        redisClient.hset(messageJson.senderId,'session_token',parsedBody.session_token);
        redisClient.hset(messageJson.senderId,'addresses','[]');
        redisClient.hset(messageJson.senderId,'state','SELECT_LOCALITY');      
        response.message = messages.enterLocality;
        return response;
      }
    }
    else{ // OTP invalid
      response.message = messages.invalidOtp ;
    }
    return response;
  });

  return responsePromise;
}

module.exports = enterOtp;