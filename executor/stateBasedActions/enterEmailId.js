// ----------------------- HEADERS ------------------
// Common to all
// TODO: abstract out all common require commands in state actions
var bluebird = require('bluebird');
var redis = require('redis');
bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);
var redisClient = redis.createClient();

var messages = require('../staticMessages/genericMessages.js');
var proxy = require('../../tinyowlProxy.js');
var tinyowlRequest = require('../../requests/tinyowlRequest.js');

// -----------------------

function enterEmailId(messageJson,sessionData){
  var response = {
    senderId: messageJson.senderId,
    platform: messageJson.platform,
    type: "PLAIN_TEXT"
  };
  var emailId = messageJson.message;
  var emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  if(!emailRegex.test(emailId)){ // invalid email case
    response.message = messages.invalidEmail;
    return response
  }

  redisClient.hset(messageJson.senderId,'email_id',emailId);
  var promise = tinyowlRequest.registerNewUser(sessionData.name,emailId,sessionData.mobile_number).then(function(res){
    redisClient.hset(messageJson.senderId,'pre_token',res.pre_token);
    redisClient.hset(messageJson.senderId,'state','ENTER_OTP');
    response.type = "ENTER_OTP";
    response.message = messages.enterOtp;
    return response;
  });
  return promise;
}

module.exports = enterEmailId;