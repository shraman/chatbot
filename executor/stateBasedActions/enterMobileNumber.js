// ----------------------- HEADERS ------------------
// Common to all
// TODO: abstract out all common require commands in state actions
var bluebird = require('bluebird');
var redis = require('redis');
bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);
var redisClient = redis.createClient();

var messages = require('../staticMessages/genericMessages.js');
var proxy = require('../../tinyowlProxy.js');
var tinyowlRequest = require('../../requests/tinyowlRequest.js');

// -----------------------

function enterMobileNumber(messageJson,sessionData){
  var response = {
    senderId: messageJson.senderId,
    platform: messageJson.platform,
    type: "PLAIN_TEXT"
  };
  var number = messageJson.message;
  if(number.match(/^[0-9]{10}$/g) !== null ){ //valid mobile number
    var responsePromise = tinyowlRequest.loginWithNumber(number).then(function(parsedBody){
      parsedBody = parsedBody.sms_login;
      if(parsedBody.registered){ // registered
        response.type = "ENTER_OTP"; // for sending resend OTP button
        redisClient.hset(messageJson.senderId,'mobile_number',number);
        redisClient.hset(messageJson.senderId,'pre_token',parsedBody.pre_token);
        redisClient.hset(messageJson.senderId,'state','ENTER_OTP');
        response.message = messages.enterOtp;
      } else { // mobile number not registered
        response.message = messages.mobileNumberNotRegistered +'\n';
        redisClient.hset(messageJson.senderId,'mobile_number',number);
        redisClient.hset(messageJson.senderId,'state','ENTER_NAME');
      }
      return response;
    });
    return responsePromise;
  } else { // invalid mobile number
    response.message = messages.mobileNumberInvalid;
    return response;
  }
}

module.exports = enterMobileNumber;
