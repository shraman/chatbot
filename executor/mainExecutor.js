var redis = require('redis');
var redisClient = redis.createClient();

var staticActions = require('./staticActions.js');
var stateBasedActions = require('./stateBasedActions/stateBasedActions.js');
var sessionActions = require('./sessionActions.js');
var sender = require('../senders/mainSender.js');
var messages = require('./staticMessages/genericMessages.js');

//message json has message, senderId and platform
function execute(messageJson){
  var message = messageJson.message;
  var senderId = messageJson.senderId;
  var response;

  //check for static response (session independent)
  response = staticActions(messageJson);
  if(response !== null){ //non empty response
    sender(response);
    return;
  }

  //session actions
  redisClient.hgetall(senderId,function(err,sessionData){
    if(sessionData !== null){ //session exists
      switch(message){
        case "restart":
        case "re":
        case "hi":
        case "hey":
        case "hello":
        case "what's up?":
        case "wassup":
          response = sessionActions.restart(messageJson,sessionData);
          sender(response);
          break;
        case "logout":
        case "lo":
          response = sessionActions.logout(messageJson,sessionData);
          sender(response);
          break;
        default:
          response = stateBasedActions(messageJson,sessionData);
          if(response.then){
            response.then(function(res){
              sender(res);
            })
            .catch(function(e){
              var response = {
                senderId: messageJson.senderId,
                platform: messageJson.platform,
                type: "PLAIN_TEXT"
              };
              if(e.error && e.error.error == "INVALID_POST_TOKEN"){
                response.message = messages.invalidOtp;
              } else{
                console.log(JSON.stringify(e));
                response.message = messages.apiError;
              }
              sender(response);
            });
          } else{       
            sender(response);
          }
      }
    } else { // session doesn't exist, create new session
      redisClient.hset(senderId,'state','WELCOME',function(err,res){
        sessionData = { state: 'WELCOME' };
        response = stateBasedActions(messageJson,sessionData);
        sender(response);
      });
    }  
  });

}

module.exports = execute;