var static_actions = require('./static_actions.js');
var stdin = process.openStdin();

process.stdout.write("Input>>");

stdin.addListener("data", function(d) {
  // note:  d is an object, and when converted to a string it will
  // end with a linefeed.  so we (rather crudely) account for that  
  // with toString() and then trim() 
  var input = d.toString().trim();
  // console.log(input);

  var message_json = {
    message: input,
    sender_id: "shraman",
    platform: "console"
  };

  static_actions.execute(message_json);

  // process.stdout.write("Input>>");

});