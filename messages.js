var messages = {
  logout_message: 'You have successfully logged out',
  help_message: 'To start your order say Hi\nTips:\n1. Type "restart" or "R" anytime to restart your order\n2. Type "logout" anytime to logout',
  welcome_message: 'Hello user, Please enter your 10-digit mobile number to initiate your order',
  mobile_number_invalid: '',
  enter_otp: '',
  invalid_otp: '',
  select_address: '',
  select_locality: ''
}

module.exports = messages;